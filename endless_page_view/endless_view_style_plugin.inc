<?php
/**
 * @file
 *  Provide the views carousel plugin object with default options and form.
 */

/**
  * Implementation of views_plugin_style().
  */
class endless_view_style_plugin extends views_plugin_style {


  function render() {
    $rows = '';

    // This will be filled in by the row plugin and is used later on in the
    // theming output.
    $this->namespaces = array();
	
	$i = 0;
    foreach ($this->view->result as $row) {
      $rows[$i] = $row->nid;
      $i++;
    }

    return theme($this->theme_functions(), $this->view, $this->options, $rows);
  }


  function options(&$options) {
    $options['startItems'] = variable_get('endless_page_startItems', 10);
    $options['groupItems'] = variable_get('endless_page_groupItems', 5);
    $options['scrollMargin'] = variable_get('endless_page_scrollMargin', 1500);
    $options['format'] = variable_get('endless_page_format', 0);
    $options['pagerType'] = variable_get('endless_page_format', 0);    
    $options['customPager'] = "More";        
    $options['loadingGraphics'] = variable_get('endless_page_loading_graphics', url('',array('absolute'=>TRUE)).drupal_get_path('module', 'endless_page')."/images/loading.gif");
  }
 
  /*
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */ 
  function options_form(&$form, &$form_state) {
    $form['pagerType'] = array(
      '#type' => 'radios',
      '#title' => t('Pager type'),
      '#options' => array(0 => t("Don't affect pager"), 1 => t('Hide pager until reaching bottom'), 2 => t('Replace pager with a custom "more" link')),
      '#default_value' => $this->options['pagerType'],
      '#required' => TRUE,            
    );  
  
      $form['customPager'] = array(
      '#type' => 'textfield',
      '#title' => t('Text of the custom "more" link'),
      '#size' => 100,
      '#maxlength' => 255,
      '#default_value' => $this->options['customPager'],
      '#description' => t("If the last option is selected in the previous question."),      
      '#required' => FALSE,            
    );
  
  
    $form['startItems'] = array(
      '#type' => 'textfield',
      '#title' => t('How many items to load when first loading the page'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['startItems'],
      '#required' => TRUE,            
    );
    
    $form['groupItems'] = array(
      '#type' => 'textfield',
      '#title' => t('How many items to load each time'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['groupItems'],
      '#required' => TRUE,            
    );    

    $form['scrollMargin'] = array(
      '#type' => 'textfield',
      '#title' => t('How far from the bottom of the page should one be before loading more items?'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['scrollMargin'],
      '#field_suffix' => ' pixels',
      '#required' => TRUE,      
    );    


    $form['format'] = array(
      '#type' => 'radios',
      '#title' => t('Node format'),
      '#options' => array(0 => t('Full node'), 1 => t('Teaser')),
      '#default_value' => $this->options['format'],
      '#required' => TRUE,            
    );

  $form['loadingGraphics'] = array(
    '#type' => 'textfield',
    '#title' => t('Loading graphics'),
    '#default_value' => $this->options['loadingGraphics'],
    '#size' => 100,
	'#maxlength' => 255,    
    '#description' => t("You can specify an image to use as loading graphics. Default is ".url('',array('absolute'=>TRUE)).drupal_get_path('module', 'endless_page')."/images/loading.gif")."<p>".t("Preview:")."<br/><img src='".$this->options['loadingGraphics']."' alt='Preview image'/>",
    '#required' => TRUE,
  );

  }

}