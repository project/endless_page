<?php
/**
 * @file
 *  Provides the views plugin information.
 */

/**
  * Implementation of hook_views_plugin().
  */
function endless_view_views_plugins() {
  return array(
    'module' => 'endless_view',
    'style' => array(
      'endless_view_style' => array(
        'title' => t('Endless View'),
        'theme' => 'endless_view_style',
        'help' => t('Makes a view that loads new content as you scroll the page.'),
        'handler' => 'endless_view_style_plugin',
        'uses row plugin' => FALSE,        
        'uses options' => TRUE,        
        'type' => 'normal',
      ),
    ),

  );
}