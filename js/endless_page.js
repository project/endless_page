
/**
 * The javascript used by endless_page module for Drupal
 * Module is written by Are Sundnes, 2008
 */

var scrollMargin;
var groupItems;
var teaser;
var nodes;
var loadPage;
var modulePath;
var xmlHttp;
var group = 0;
var item = 0;
var itemid;
var loadingGroup = false;
var loadingGraphics;
var pagerElement;
var pagerType;
var customPager;
var currentPage;



	var qsParm = new Array();
	function qs() {
		var query = window.location.search.substring(1);
		var parms = query.split('&');
		for (var i=0; i<parms.length; i++) {
			var pos = parms[i].indexOf('=');
			if (pos > 0) {
				var key = parms[i].substring(0,pos);
				var val = parms[i].substring(pos+1);
				qsParm[key] = val;
			}
		}	
	} 
	
	qsParm['page'] = null;
	qs();	



    function getPager(hide) {
    if(!pagerElement){
		for(i=0;i<document.getElementById("content").getElementsByTagName("ul").length;i++) {
			//Get ul tags in #content with class name "pager"
			if (document.getElementById("content").getElementsByTagName("ul")[i].className=="pager") {
				pagerElement = document.getElementById("content").getElementsByTagName("ul")[i];
				if(hide) pagerElement.style.display='none';
			}
		}
	} else {
		if(hide) pagerElement.style.display='none';	
    }
    }

	function showPager() {
		if (pagerType==2) {
						
			if(location.href.indexOf("?")>-1) url =location.href.split("?")[0];
			else url = location.href;


			currentPage = Number(qsParm['page']);
			if(currentPage) nextPage = currentPage+1;
			else nextPage = 1;	
			
			pagerElement.innerHTML="<li class='endless-pager'><a href='"+url+"?page="+nextPage+"'/>"+customPager+"</a></li>"; 	

		}
		
		pagerElement.style.display='block';
	}
	


// inputNodes = Array of nodeID's to load content from.
// inputStartItmes = Number of items to load on page initiation
// inputScrollMargin = Number of pixels from the bottom of the document onw should scroll before loading next group
// inputTeaser = Should the nodes load as teasers or full nodes? Allowed values: 1 and 0.
// inputLoadPage = Complete URL to the loadpage created by the module
// inputModulePath = Complete URL to the endless_page module folder
// inputLoadingGraphics = Image to show wile loading
// inputPagerType = This is only used with Endless Views. 0 is "Don't affect pager", 1 is "Hide pager until reaching bottom" 2 is "Use custom pager".
function init_endless_page(inputNodes,inputStartItems,inputScrollMargin,inputGroupItems,inputTeaser,inputLoadPage,inputModulePath,inputLoadingGraphics,inputPagerType,inputCustomPager, inputCurrentPage) {

	// Set the variables
	nodes=inputNodes;
	scrollMargin = inputScrollMargin;
	teaser = inputTeaser;
	loadPage = inputLoadPage;
	modulePath = inputModulePath;
	loadingGraphics = inputLoadingGraphics;	
	pagerType = Number(inputPagerType);
	customPager = inputCustomPager;
	currentPage = inputCurrentPage;

	// Hide pager in view
	if(pagerType>0) getPager(true);
	
	// Set groupItems to inputStartItems for loading the first group
	itemid = inputStartItems; 
	
	// The first group is loaded, so now set groupItems to inputGroupItems
	groupItems = inputGroupItems; 	
		
	// Use jQuery Dimensions to get scrollbar position
	// This function is run each time the user changes the scroll position

	window.onload = function() {	
		if(pagerType>0) getPager(true);
	}
	// Hide pager in view
	if(pagerType>0) getPager(true);

		$(window).scroll( function() { 
		
			// Check if user is scrolled further down than scrollMargin	
			if($(document).scrollTop()>=(($(document).height()-$(window).height())-scrollMargin)) {
				// If scrolled further down than scrollMargin and the script is not allready loading another group and the script is not stopped, load the next group.
				if(!loadingGroup) endless_page_load();	
			}			
		} );
		if($(document).scrollTop()>=(($(document).height()-$(window).height())-50)) {
			endless_page_load();
		}
	}



// Function for loading the next group
function endless_page_load() 
{
	if(pagerType>0) getPager(true);
	if(!nodes[item]) {
		// Show pager
		if(pagerType>0) showPager(customPager);
		return;
	}
		// Create the div for the new group
		var newGroup = document.createElement("div"); 
		
		// Set ID to the div
		newGroup.setAttribute("id","endless-group-"+group); 
		
		// Insert the preloader gif
		newGroup.innerHTML="<img src='"+loadingGraphics+"'/>"; 

		// Insert new group on the end of the container DIV
		document.getElementById("endless_page_loaded").parentNode.insertBefore(newGroup,document.getElementById("endless_page_loaded")); 

		// Setting this variable to TRUE will keep the javascript from attempting to load any more nodes. We will unset it once we get the content from this call.
		loadingGroup=true; 

		// Load the xmlHttp object required for using AJAX 
		xmlHttp=GetXmlHttpObject(); 
		if (xmlHttp==null) {
			// If javascript can't get the xmlHttp object, the user will not be able to use this page.
			alert ("Your browser does not support AJAX!"); 
			return;
		} 

	// Start making the URL for the load page
	var url=loadPage; 
	
	// The first argument to the load page is the current item number. This is used to make an unique div id for each item. May be useful.
	url += "/"+itemid; 

	// Tell the load page to load as teaser or as full page
	url += "/"+teaser; 

	// Loop this for each item in group
	var i = groupItems;	
	while(i>0) {	

		// Add the item's nodeID as an argument for the load page.
		url+="/"+nodes[item]; 

		item++;
		itemid++
		i--;
	}

	xmlHttp.onreadystatechange=stateChanged; 
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
} 

// Function for telling the javascript it can start loading new nodes again. If scrolled all the way to the bottom, we will load the next one right away.
function restartFunction() {
	loadingGroup=false; 
	if($(document).scrollTop()>=(($(document).height()-$(window).height())-50)) {
		endless_page_load()
	}	
}


//xmlHttp state has changed
function stateChanged() 
{ 
	// State is 4 - meaning the content was recieved successfully.
	if (xmlHttp.readyState==4) 
	{ 
		// Set the content of the group now that we got it back from the load page.
		document.getElementById("endless-group-"+group).innerHTML=xmlHttp.responseText; 

		// If there are any more nodes in the array, unset this variable to let javascript know we're finished loading for now and it can run the function again, but wait a few seconds, so it wont start loading more nodes right away
		if(nodes[item]) setTimeout('restartFunction()',2000);

		// Show pager
		else if(pagerType>0) showPager(customPager);

		group++;
	}
}


// Standard AJAX function for getting the xmlHttpObject. Different browsers handles this differently - thus the try and catch.
function GetXmlHttpObject() 
{
	var xmlHttp=null;
	try {
	  // Firefox, Opera 8.0+, Safari
	  xmlHttp=new XMLHttpRequest(); }
	catch (e) {
	  // Internet Explorer
	  try { xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); }
	  catch (e) { xmlHttp=new ActiveXObject("Microsoft.XMLHTTP"); }
	}
	return xmlHttp;
}